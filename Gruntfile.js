module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    compass: {
      main: {
        options: {
          bundleExec: true,
          config: 'compass.rb',
        },
        files: {
          'css/application.css': ['scss/application.scss']
        }
      },
    },
    watch: {
      compass: {
        files: 'scss/*.scss',
        tasks: ['compass']
      },
    },
    clean: ["css/*"]
  });

  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-clean');

  // Default task(s).
  grunt.registerTask('default', ['build']);
  grunt.registerTask('build', ['compass']);


};
